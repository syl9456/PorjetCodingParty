all : initial chefs mecanos clients


initial : initial.c types.h
	gcc -Wall initial.c -o initial

clients : clients.c types.h
	gcc -Wall clients.c -o clients

chefs : chefs.c types.h
	gcc -Wall chefs.c -o chefs

mecanos : mecanos.c types.h
	gcc -Wall mecanos.c -o mecanos




clean : 
	rm -f initial.o initial mecanos.o mecanos chefs.o chefs clients.o clients cleFile1.serv cleFile2.serv cleSegOut.serv cleSegPrio.serv
