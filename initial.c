/**********************************************************************************
 * 
 * Projet de Sylvain Rome 
 * pour Programmation systeme 
 * DATE : 16 DECEMBRE 2021
 * 
 * *******************************************************************************/

#include "types.h"

int tab_outils[4];
int file_messCM;								// ID de le file de messages           
int file_messCC;
int *adr_SEG_outils;							// @ attachement SMP   
int *adr_SEG_prio;               
int semap;           							// ID de l'ES                          
int mem_part_outils;							// ID du SMP     
int mem_part_prio;                      




Tableau_chefs tab_chefs;
Tableau_mecanos tab_mecanos; 
Tableau_clients tab_clients;
int nb_chefs=0,nb_mecanos=0,nb_clients=0;



void usage(char *s){
	couleur(REINIT);
	fprintf(stdout," Usage : %s [nb_chefs] [nb_mecanos > 2] [nb-outil1] [nb-outil2] [nb-outil3] [nb-outil4]\n", s);
	exit(EXIT_FAILURE);
}


void tests_arguments(int argc, char * argv[]){
	if(argc != 7 ){
		usage(argv[0]);
	}
	if(atoi(argv[2]) < 2 ){
		usage(argv[0]);
	}

	for(int i= 1;i >7; i++){
		if(atoi(argv[i]) < 0 ){
			usage(argv[0]);
		}
	}
}

void mailing(int Signal){
	int i;
	for(i=0;i<nb_chefs;i++){
		kill(tab_chefs[i],Signal);
	}
	for(i=0;i<nb_mecanos;i++){
		kill(tab_mecanos[i],Signal);
	}
	for(i=0;i<nb_clients;i++){
		kill(tab_clients[i],Signal);
	}
}


void arret(int s){
	couleur(ROUGE);
	fprintf(stdout,"\n\t\t\t\t\tFERMETURE DU GARAGE\n\t\t\t\t\t   (sigusr1 recu)\n\n");
    couleur(REINIT);
    mailing(SIGKILL);
    shmdt(adr_SEG_outils);
    shmdt(adr_SEG_prio);
    shmctl(mem_part_prio,IPC_RMID,NULL);
	shmctl(mem_part_outils,IPC_RMID,NULL);
	semctl(semap,1,IPC_RMID,NULL);
	msgctl(file_messCM,IPC_RMID,NULL);
	msgctl(file_messCC,IPC_RMID,NULL);
    exit(EXIT_SUCCESS);
}

void mon_sigaction(int signal, void (*f)(int)){
    struct sigaction action;
    action.sa_handler = f;
    action.sa_flags = 0;
    sigaction(signal,&action,NULL);
}




int main(int argc, char * argv[], char ** env){
	struct stat st;								// pour le stat sur le fichier cle
	key_t cleCM;								// cle pour la file Chef Mecano 
	key_t cleCC;								// cle pour la file Chef Client
	key_t cleSegOutils;							// cle le segment de memoire partagée
	key_t cleSegPrio;
	pid_t pid;



	int nb_chefs,nb_mecanos;											//Bureaucratie...
	int i;
	char num[1024],out1[1024],out2[1024],out3[1024],out4[1024];
	char nb_chefs_char[1024],cle_CC_char[1024],cle_SegPrio_char[1024];
	unsigned short val_init[1]={1};


	tests_arguments(argc,argv);		// Assignment des arguments dans des variables
	nb_chefs = atoi(argv[1]);
	nb_mecanos = atoi(argv[2]);

	for(int i=3; i<7; i++){
		tab_outils[i-3] = atoi(argv[i]);
	}

	couleur(ROUGE);
	fprintf(stdout,"\n\t\t\t\t\tOUVERTURE DU GARAGE\n\n\n");



	/****************************************************************************
	 * 							CREATION DE LA CLE
	***************************************************************************/

	/* On teste si le fichier cle existe dans le repertoire courant : */
	if ((stat(FICHIER_CLE_SEG_OUT,&st) == -1) && (open(FICHIER_CLE_SEG_OUT, O_RDONLY | O_CREAT | O_EXCL, 0660) == -1)){
		couleur(REINIT);
		fprintf(stderr,"Pas de fichier cle et pb creation fichier cle, bye\n");
		exit(-1);
    }
    if ((stat(FICHIER_CLE_SEG_PRIO,&st) == -1) && (open(FICHIER_CLE_SEG_PRIO, O_RDONLY | O_CREAT | O_EXCL, 0660) == -1)){
		couleur(REINIT);
		fprintf(stderr,"Pas de fichier cle et pb creation fichier cle, bye\n");
		exit(-1);
    }
    if ((stat(FICHIER_CLE_FILECM,&st) == -1) && (open(FICHIER_CLE_FILECM, O_RDONLY | O_CREAT | O_EXCL, 0660) == -1)){
    	couleur(REINIT);
		fprintf(stderr,"Pas de fichier cle et pb creation fichier cle, bye\n");
		exit(-1);
    }
    if ((stat(FICHIER_CLE_FILECC,&st) == -1) && (open(FICHIER_CLE_FILECC, O_RDONLY | O_CREAT | O_EXCL, 0660) == -1)){
    	couleur(REINIT);
		fprintf(stderr,"Pas de fichier cle et pb creation fichier cle, bye\n");
		exit(-1);
    }

    cleSegOutils = ftok(FICHIER_CLE_SEG_OUT,'a');
    if (cleSegOutils==-1){
    	couleur(REINIT);
		fprintf(stderr,"Pb creation cle\n");
		exit(-1);
    }

    cleSegPrio = ftok(FICHIER_CLE_SEG_PRIO,'a');
    if (cleSegPrio==-1){
    	couleur(REINIT);
		fprintf(stderr,"Pb creation cle\n");
		exit(-1);
    }

    cleCM = ftok(FICHIER_CLE_FILECM,'a');
    if (cleCM==-1){
    	couleur(REINIT);
		fprintf(stderr,"Pb creation cle\n");
		exit(-1);
    }

    cleCC = ftok(FICHIER_CLE_FILECC,'a');
    if (cleCC==-1){
    	couleur(REINIT);
		fprintf(stderr,"Pb creation cle\n");
		exit(-1);
    }


    /****************************************************************************
     * 						CREATION DE LA FILE DE MESSAGES
	****************************************************************************/


    file_messCM = msgget(cleCM,IPC_CREAT | IPC_EXCL | 0660);
    if (file_messCM==-1){
    	couleur(REINIT);
		fprintf(stderr,"Pb creation file de message 1\n");
		exit(-1);
    }

    file_messCC = msgget(cleCC,IPC_CREAT | IPC_EXCL | 0660);
    if (file_messCC==-1){
    	couleur(REINIT);
		fprintf(stderr,"Pb creation file de message 2\n");
		exit(-1);
    }

    /****************************************************************************
  	*						CREATION DU SEGMENT DE MEMOIRE PARTAGEE
	*							POUR LES OUTILS ET SEMAPHORE
	****************************************************************************/

    // On cree le SMP et on teste s'il existe deja 
    mem_part_outils = shmget(cleSegOutils,sizeof(int[4]),IPC_CREAT | IPC_EXCL | 0660);
    if (mem_part_outils == -1){
    	couleur(REINIT);
		fprintf(stderr,"Pb creation SMP ou il existe deja\n");
		exit(-1);
    }

    mem_part_prio = shmget(cleSegPrio,sizeof(int[nb_chefs]) + sizeof(pid_t[nb_chefs]),IPC_CREAT | IPC_EXCL | 0660);
    if (mem_part_prio == -1){
    	couleur(REINIT);
		fprintf(stderr,"Pb creation SMP ou il existe deja\n");
		exit(-1);
    }

    // Attachement de la memoire partagee
    adr_SEG_outils = shmat(mem_part_outils,NULL,0);
    if (adr_SEG_outils == (int *) -1){
    	couleur(REINIT);
		fprintf(stderr,"Pb attachement\n");
		shmctl(mem_part_outils,IPC_RMID,NULL);			// Il faut detruire le SMP puisqu'on l'a cree
		exit(-1);
    }

    adr_SEG_prio = shmat(mem_part_prio,NULL,0);
    if (adr_SEG_prio == (int *) -1){
    	couleur(REINIT);
		fprintf(stderr,"Pb attachement\n");
		shmctl(mem_part_prio,IPC_RMID,NULL);			// Il faut detruire le SMP puisqu'on l'a cree
		exit(-1);
    }


    semap = semget(cleSegOutils,4,IPC_CREAT | IPC_EXCL | 0660);
    if (semap == -1){
    	couleur(REINIT);
		fprintf(stderr,"Pb creation ensemble de semaphore ou il existe deja\n");
		shmctl(mem_part_outils,IPC_RMID,NULL);		// Il faut detruire le SMP puisqu'on l'a cree

		/* Le detachement du SMP se fera a la terminaison */
		exit(-1);
    }

    // On l'initialise 
    if (semctl(semap,4,SETALL,val_init) == -1){
    	couleur(REINIT);
		fprintf(stderr,"Pb initialisation semaphore\n");
		semctl(semap,4,IPC_RMID,NULL);		// On detruit les IPC deja crees
		shmctl(mem_part_outils,IPC_RMID,NULL);
		exit(-1);
    }

    //On remplis le segment de memoire partagée avec le nombre d'outils possibles
    adr_SEG_outils[0] = tab_outils[0];
    adr_SEG_outils[1] = tab_outils[1];
	adr_SEG_outils[2] = tab_outils[2];
	adr_SEG_outils[3] = tab_outils[3];

	for(i=0;i<nb_chefs;i++){
		adr_SEG_prio[i] = 0;
	}


	/****************************************************************************
	*								CREATION DES PROCESSUS
	****************************************************************************/
	/****************************************************************************
	*									CHEFS
	****************************************************************************/

	couleur(REINIT);
	couleur(ROUGE);
	fprintf(stdout,"\nCreation des chefs : \n");
	for (i=0;i<nb_chefs;i++){
		couleur(REINIT);
		couleur(ROUGE);
		fprintf(stdout,"\t(initialisation) Creation chef %d\n",i);

		// On met toutes les varaibles voulues en char pour les faire passer dans les arguments de execl
		sprintf(num,"%d",i);
		sprintf(out1,"%d",tab_outils[0]);
		sprintf(out2,"%d",tab_outils[1]);
		sprintf(out3,"%d",tab_outils[2]);
		sprintf(out4,"%d",tab_outils[3]);

		pid = fork();
		if (pid == -1){
			couleur(REINIT);
	    	fprintf (stderr,"\t(initialisation) Fork a échoué, chef non créé\n ");
	    	fprintf (stderr,"i = %d\n",i);
		} 
		else if (pid == 0){// fils numero i+1
			usleep(1000);
			couleur(REINIT);
			couleur(ROUGE);
			fprintf (stdout,"\t(initialisation) Lancement du chef %d;\n ",i);
	    	execl("chefs","chefs",num,out1,out2,out3,out4,env);   	//Execl des chefs 
	    	/* JAMAIS ATTEINT    */
	    	exit(-1);
		}else{ 
	    	adr_SEG_prio[nb_chefs+i] = pid;						//On remplis le tab des chefs
	    	tab_chefs[i] = pid;
	    	/* Attente de la reponse des procs fils   */
    	}
    	usleep(500000);
    }
    usleep(1000000);

    /****************************************************************************
	*								MECANOS
	****************************************************************************/
	
	couleur(REINIT);
    couleur(ROUGE);
	fprintf(stdout,"\n Creation des mecanos : \n");
	for (i=0;i<nb_mecanos;i++){
		couleur(REINIT);
		couleur(ROUGE);
		fprintf (stdout,"\t(initialisation) Creation mecano %d\n",i);

		// On met toutes les variables voulues en char pour les faire passer dans les arguments de execl
		sprintf(num,"%d",i);

		pid = fork();
		if (pid == -1){
			couleur(REINIT);
	    	fprintf (stderr,"\t(initialisation) Fork a échoué, mecanos non créé\n ");
	    	fprintf (stderr,"i = %d\n",i);
		} 
		else if (pid == 0){// fils numero i+1
			usleep(1000);
			couleur(REINIT);
			couleur(ROUGE);
			fprintf (stdout,"\t(initialisation) Lancement du mecano %d; \n ",i);
	    	execl("mecanos","mecanos",num,NULL);		//Execl des mecanos
	    	/* JAMAIS ATTEINT    */
	    	exit(-1);
		}else{
	    	tab_mecanos[i] = pid;				//On remplis le tab des mecanos
	    	/* Attente de la reponse des procs fils   */
		}
		usleep(500000);
    }
    usleep(1000000);


    /****************************************************************************
	*								CLIENTS
	****************************************************************************/
	
	mon_sigaction(SIGUSR1,arret);
	couleur(REINIT);
	couleur(ROUGE);
    fprintf(stdout,"\nCreation des clients : \n");
    for(i=0;;){
    	couleur(REINIT);
    	couleur(ROUGE);
    	fprintf (stdout,"\t(initialisation) Creation client %d\n",i);
    	sprintf(num,"%d",i);
    	sprintf(nb_chefs_char,"%d",nb_chefs);
    	sprintf(cle_CC_char,"%d",cleCC);
    	sprintf(cle_SegPrio_char,"%d",cleSegPrio);
		pid = fork();
		if (pid == -1){
	    	fprintf (stderr,"\t(initialisation) Fork a échoué, client non créé\n ");
	    	fprintf (stderr,"i = %d\n",i);
		}
		else if (pid == 0){// fils numero i+1
			usleep(1000);
			couleur(REINIT);
			couleur(ROUGE);
			fprintf (stdout,"\t(initialisation) Lancement du client %d; \n ",i);
	    	execl("clients","clients",num,nb_chefs_char,cle_CC_char,cle_SegPrio_char,env);   //Execl des clients
	    	/* JAMAIS ATTEINT    */
	    	exit(-1);
	    }else{
	    	tab_clients[i] = pid;	//On remplis le tab des clients
	    	nb_clients++;
	    	/* Attente de la reponse des procs fils   */
	    	i++;
		}
		usleep(10000000);
    }
	exit(0);
}