/**********************************************************************************
 * 
 * Projet de Sylvain Rome 
 * pour Programmation systeme 
 * DATE : 16 DECEMBRE 2021
 * 
 * *******************************************************************************/


#include "types.h"


/****************************************************************************
*                         FONCTIONS POUR DEBUT PROGRAMME
****************************************************************************/

void usage(char *s){
    couleur(REINIT);
    fprintf(stdout," \nUsage : %s [n° d'ordre] [nb outil1] [nb outil2] [nb outil3] [nb outil4] \n",s);
    exit(EXIT_FAILURE);
}

void tests_arguments(int argc, char * argv[]){
    if(argc != 7 ){
        usage(argv[0]);
    }
}

/****************************************************************************
*                         FONCTIONS POUR SIGACTION
****************************************************************************/

void arret(int s){
    fprintf(stdout,"Chef s'arrete  (sigusr1 recu)\n");
    exit(EXIT_SUCCESS);
}


void mon_sigaction(int signal, void (*f)(int)){
    struct sigaction action;
    action.sa_handler = f;
    action.sa_flags = 0;
    sigaction(signal,&action,NULL);
}


/****************************************************************************
*                                MAIN
****************************************************************************/

int main(int argc, char * argv[], char ** env){
    srand(getpid());
    struct stat st;                         // pour le stat sur le fichier cle      
    key_t cleCM;                            // cle pour file Chef Mecano 
    key_t cleCC;                            // cle pour file Chef Client
    int file_messCM;                          // ID de la file Chef mecano  
    int file_messCC;                          // ID de la file Chef Client  

    requete_travail_de_chef_t req_travail;      // Structure de requete pour la file chef mecano 
    reponse_travail_de_mecanos_t rep_travail;   // Structure de reponse pour la file chef mecano 
    requete_de_client_t req_client;             // Structure de requete pour la file chef Client 
    reponse_client_t rep_client;                // Structure de reponse pour la file chef Client 

    tests_arguments(argc,argv);


    int pid = getpid();
    int num_ordre = atoi(argv[1]);
    int tab_outils[4];
    int i;

    couleur(JAUNE);
    fprintf(stdout,"\t\t(Chef) Chef Jerry Kan n°%d pret ! Mon pid : %d\n",num_ordre,pid);

    /****************************************************************************
    *                         REMPLISSAGE TAB OUTILS
    ****************************************************************************/

    for(i=0;i<4;i++){
        tab_outils[i] = atoi(argv[i+2]);
    }

    /****************************************************************************
    *                         RECUPARATION DES CLES
    ****************************************************************************/

    
    if ((stat(FICHIER_CLE_FILECM,&st) == -1) && (open(FICHIER_CLE_FILECM, O_RDONLY | O_CREAT | O_EXCL, 0660) == -1)){
        couleur(REINIT);
        fprintf(stderr,"Pas de fichier cle et pb creation fichier cle, bye\n");
        exit(-1);
    }

    if ((stat(FICHIER_CLE_FILECC,&st) == -1) && (open(FICHIER_CLE_FILECC, O_RDONLY | O_CREAT | O_EXCL, 0660) == -1)){
        couleur(REINIT);
        fprintf(stderr,"Pas de fichier cle et pb creation fichier cle, bye\n");
        exit(-1);
    }
    
    cleCM = ftok(FICHIER_CLE_FILECM,'a');
    if (cleCM==-1){
        couleur(REINIT);
        fprintf(stderr,"Pb creation cle\n");
        exit(-1);
    }

    cleCC = ftok(FICHIER_CLE_FILECC,'a');
    if (cleCC==-1){
        couleur(REINIT);
        fprintf(stderr,"Pb creation cle\n");
        exit(-1);
    }

    /****************************************************************************
    *                         RECUPERATION FILES DE MESSAGES
    ****************************************************************************/

    //Recuperation file de message 
    file_messCM = msgget(cleCM,0);
    if (file_messCM==-1){
        couleur(REINIT);
        fprintf(stderr,"Pb recuperation file de message\n");
        exit(-1);
    }

    file_messCC = msgget(cleCC,0);
    if (file_messCC==-1){
        couleur(REINIT);
        fprintf(stderr,"Pb recuperation file de message\n");
        exit(-1);
    }


    mon_sigaction(SIGUSR1,arret);
    /****************************************************************************
    *                             BOUCLE D'ATTENTE
    ****************************************************************************/
    for(;;){

        /****************************************************************************
        *                           si recu message dans la file
        *                               client -> chef
        ****************************************************************************/
        if((msgrcv(file_messCC,&req_client,sizeof(requete_de_client_t)-sizeof(long),pid,IPC_NOWAIT)) == -1){
            if(errno != ENOMSG){
                couleur(REINIT);
                fprintf(stderr,"(Chef) messages pas recus\n");
                raise(SIGUSR1);
            }
        } else{
            couleur(JAUNE);
            fprintf(stdout,"\t\t(Chef) Chef n°%d : recoit un message de %d \n",num_ordre,req_client.client);

            //  remplissage requete pour les mecanos
            req_travail.type = 1;
            req_travail.chef = pid;
            req_travail.duree = rand() % 6;
            req_travail.client = req_client.client;
            req_travail.outil[0] = rand() % (tab_outils[0]/2)+1;
            req_travail.outil[1] = rand() % (tab_outils[1]/2)+1;
            req_travail.outil[2] = rand() % (tab_outils[2]/2)+1;
            req_travail.outil[3] = rand() % (tab_outils[3]/2)+1;

            /****************************************************************************
            *                           Envoi message dans la file
            *                               chef -> mecano
            ****************************************************************************/

            couleur(JAUNE);
            fprintf(stdout,"\t\t(Chef) Chef n°%d, envoie une requete pour du travail \n",num_ordre);
            if((msgsnd(file_messCM,&req_travail,sizeof(requete_travail_de_chef_t)-sizeof(long),0)) != -1){
                if(errno != ENOMSG){
                    couleur(JAUNE);
                    fprintf(stdout,"\t\t(Chef) Message envoyé a mecano avec succes ! \n");
                }
            }
        }


        /****************************************************************************
        *                           si recu message dans la file
        *                               chef -> mecano
        ****************************************************************************/
        if((msgrcv(file_messCM,&rep_travail,sizeof(reponse_travail_de_mecanos_t)-sizeof(long),pid,IPC_NOWAIT)) == -1){
            if(errno != ENOMSG){
                couleur(REINIT);
                fprintf(stderr,"(Chef) messages pas recus\n");
                raise(SIGUSR1);
            }
        }else {
            couleur(JAUNE);
            fprintf(stdout,"\t\t(Chef) Chef n°%d : recoit un message de %d \n",num_ordre,rep_travail.mecano);

            //   remplissage requete pour les clients
            rep_client.type = rep_travail.client;
            rep_client.mecano = rep_travail.mecano;

            /****************************************************************************
            *                           Envoi message dans la file
            *                               client -> chef
            ****************************************************************************/
            couleur(JAUNE);
            fprintf(stdout,"\t\t(Chef) Chef n°%d, envoie une reponse au client %d \n",num_ordre,rep_travail.client);
            if((msgsnd(file_messCC,&rep_client,sizeof(reponse_client_t)-sizeof(long),IPC_NOWAIT)) != -1){
                if(errno != ENOMSG){
                    couleur(JAUNE);
                    fprintf(stdout,"\t\t(Chef) Message envoyé a client avec succes ! \n");
                    couleur(REINIT);
                }
            }    
        }
    }


    exit(0);
}