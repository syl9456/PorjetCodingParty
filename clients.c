/**********************************************************************************
 * 
 * Projet de Sylvain Rome 
 * pour Programmation systeme 
 * DATE : 16 DECEMBRE 2021
 * 
 * *******************************************************************************/

#include "types.h"

int *adr_SEG_prio;                                   // ID pour le segment de memmoire partagée
int mem_part_prio;                                   // @ pour le segment de memoire partagée


/****************************************************************************
*                         FONCTIONS POUR DEBUT PROGRAMME
****************************************************************************/

void usage(char *s){
    couleur(REINIT);
    fprintf(stdout," Usage : %s [n° d'ordre] [nb outil1] [nb outil2] [nb outil3] [nb outil4] \n",s);
    exit(EXIT_FAILURE);
}


void tests_arguments(int argc, char * argv[]){
    if(argc != 7){
        usage(argv[0]);
    }
}

/****************************************************************************
*                         FONCTIONS POUR SIGACTION
****************************************************************************/

void arret(int s){
    couleur(REINIT);
    fprintf(stdout,"Client s'arrete  (sigusr1 recu)\n");
    exit(EXIT_SUCCESS);
}


void mon_sigaction(int signal, void (*f)(int)){
    struct sigaction action;
    action.sa_handler = f;
    action.sa_flags = 0;
    sigaction(signal,&action,NULL);
}



int cherche_moins_occupe(int nb_chefs){
    int i;
    int val_moins_occupe=1000;
    int position_moins_occupe=0;

    for(i=0;i<nb_chefs;i++){
        if(adr_SEG_prio[i]<val_moins_occupe){
            val_moins_occupe = adr_SEG_prio[i];
            position_moins_occupe = i;
        }
    }
    return position_moins_occupe;
}


/****************************************************************************
*                                MAIN
****************************************************************************/


int main(int argc, char * argv[], char ** env){
    int file_messCC;                                // ID pour la file de messsages Chef mecano

    tests_arguments(argc,argv);

    key_t cleCC = atoi(argv[3]);                    // Cle pour la file de messages Chef Mecano
    key_t cleSegPrio = atoi(argv[4]);
    requete_de_client_t req_client;             // Structure de requete pour la file chef Client 
    reponse_client_t rep_client;                // Structure de reponse pour la file chef Client 



    int pid = getpid();
    int nb_chefs = atoi(argv[2]);
    int num_ordre = atoi(argv[1]);
    int num_chef;

    couleur(VERT);
    fprintf(stdout,"\n\t(Client) Client Sarah Vigote n°%d pret ! Mon pid : %d\n",num_ordre,pid);




    /****************************************************************************
    *                         RECUPERATION FILES DE MESSAGES
    ****************************************************************************/

    file_messCC = msgget(cleCC,0);
    if (file_messCC==-1){
        couleur(REINIT);
        fprintf(stderr,"Pb recuperation file de message\n");
        raise(SIGUSR1);
    }


    /****************************************************************************
    *                    RECUPERATION DU SEGMENT DE MEMOIRE
    ****************************************************************************/


    // On cree le SMP et on teste s'il existe deja et on met son ID dans mem_part
    mem_part_prio = shmget(cleSegPrio,sizeof(int[nb_chefs])+sizeof(pid_t[nb_chefs]),0);
    if (mem_part_prio == -1){
        couleur(REINIT);
        fprintf(stderr,"Pb recuperataion SMP\n");
        raise(SIGUSR1);
    }

    // Attachement de la memoire partagee et on met son @ dans adr_SEG    
    adr_SEG_prio = shmat(mem_part_prio,NULL,0);
    if (adr_SEG_prio == (int *) -1){
        couleur(REINIT);
        fprintf(stderr,"Pb attachement memoire\n");
        shmctl(mem_part_prio,IPC_RMID,NULL);         // Il faut detruire le SMP puisqu'on l'a cree
        raise(SIGUSR1);
    }



    /****************************************************************************
    *                       RECHERCHE DU CHEF LE MOIN OCCUPE
    ****************************************************************************/


    num_chef = cherche_moins_occupe(nb_chefs);

    mon_sigaction(SIGUSR1,arret);

    req_client.client = pid;
    req_client.type = adr_SEG_prio[num_chef+nb_chefs];

    if((msgsnd(file_messCC,&req_client,sizeof(requete_de_client_t)-sizeof(long),0)) != -1){
        couleur(VERT);
        fprintf(stdout,"\t(Client) Message envoyé chefs n°%d avec succes ! File de prio : %d \n",num_chef,adr_SEG_prio[num_chef]);
        adr_SEG_prio[num_chef] += 1;
    }


    /****************************************************************************
    *                            BOUCLE D'ATTENTE
    ****************************************************************************/

    for(;;){
        if ((msgrcv(file_messCC,&rep_client,sizeof(reponse_client_t)-sizeof(long),pid,0)) == -1){
            couleur(REINIT);
            fprintf(stderr,"Erreur de lecture, erreur %d\n",errno);
            raise(SIGUSR1);
        }else {
            
            couleur(VERT);
            fprintf(stdout,"\n(Client) Probleme de Client %d : RESOLU! par mecano %d!\tNICKEL L'AUTO !! \n",num_ordre,rep_client.mecano);
            fprintf(stdout,"Le Client %d s'en va! Ciao Sarah Vigote !\n\n\n",num_ordre);
            adr_SEG_prio[num_chef] -= 1;
            exit(0);

        }
    }

    exit(0);
}
