/**********************************************************************************
 * 
 * Projet de Sylvain Rome 
 * pour Programmation systeme 
 * DATE : 16 DECEMBRE 2021
 * 
 * *******************************************************************************/

#include "types.h"

int *adr_SEG;                                   // ID pour le segment de memmoire partagée
int mem_part;                                   // @ pour le segment de memoire partagée


/****************************************************************************
*                       FONCTIONS POUR LES SEMAPHORES
****************************************************************************/

int O(int semaphoreID,int semaphore,int n){
    struct sembuf op = {semaphore, -n, SEM_UNDO};
    return semop(semaphoreID, &op, 1);
}
int F(int semaphoreID,int semaphore,int n){
    struct sembuf op = {semaphore, n, SEM_UNDO};
    return semop(semaphoreID, &op, 1);
}


/****************************************************************************
*                         FONCTIONS POUR DEBUT PROGRAMME
****************************************************************************/

void usage(char *s){
    couleur(REINIT);
    fprintf(stdout,"Usage : %s [n° d'ordre] [nb outil1] [nb outil2] [nb outil3] [nb outil4] \n",s);
    exit(EXIT_FAILURE);
}


void tests_arguments(int argc, char * argv[]){
    if(argc != 2){
        usage(argv[0]);
    }
}


/****************************************************************************
*                         FONCTIONS POUR SIGACTION
****************************************************************************/

void arret(int s){
    couleur(REINIT);
    fprintf(stdout,"Mecano s'arrete  (sigusr1 recu)\n");
    exit(EXIT_SUCCESS);
}


void mon_sigaction(int signal, void (*f)(int)){
    struct sigaction action;
    action.sa_handler = f;
    action.sa_flags = 0;
    sigaction(signal,&action,NULL);
}


/****************************************************************************
*                        		 MAIN
****************************************************************************/


int main(int argc, char * argv[], char ** env){
	struct stat st;									// Structure stat pour vérifier les fichiers plus bas  
	int file_messCM;								// ID pour la file de messsages Chef mecano
	key_t cleCM;									// Cle pour la file de messages Chef Mecano
	key_t cleSeg;									// Cle pour le segment de memoire partagée
	int semap;									    // ID pour la semaphore

    tests_arguments(argc,argv);

	int tout_bon = 0;								// Bureaucratie...
	int outils_requis[4];
	int duree_requise;
	int pid = getpid();
	int num_ordre = atoi(argv[1]);

	requete_travail_de_chef_t req_travail;			// Structure de requete pour la file chef mecano
    reponse_travail_de_mecanos_t rep_travail;		// Structure de reponse pour la file chef mecano 

    couleur(BLEU);
    fprintf(stdout,"\t\t\t(Mecano) Mecano Terry Dicule n°%d pret ! Mon pid : %d\n",num_ordre,pid);


	/****************************************************************************
    *                         Récupération de la clé de File
    ****************************************************************************/


	if ((stat(FICHIER_CLE_FILECM,&st) == -1) && (open(FICHIER_CLE_FILECM, O_RDONLY | O_CREAT | O_EXCL, 0660) == -1)){
        couleur(REINIT);
        fprintf(stderr,"Pas de fichier cle et pb creation fichier cle, bye\n");
        raise(SIGUSR1);
    }

    if ((stat(FICHIER_CLE_SEG_OUT,&st) == -1) && (open(FICHIER_CLE_SEG_OUT, O_RDONLY | O_CREAT | O_EXCL, 0660) == -1)){
        couleur(REINIT);
        fprintf(stderr,"Pas de fichier cle et pb creation fichier cle, bye\n");
        raise(SIGUSR1);
    }

    // Récuperation de la clé de file de messages Chef Mecano 
    cleCM = ftok(FICHIER_CLE_FILECM,'a');
    if (cleCM==-1){
        couleur(REINIT);
        fprintf(stderr,"Pb creation cleCM\n");
        raise(SIGUSR1);
    }

    // Récuperation de la clé de Segment
    cleSeg = ftok(FICHIER_CLE_SEG_OUT,'a');
    if (cleSeg==-1){
        couleur(REINIT);
        fprintf(stderr,"Pb creation cleCC\n");
        raise(SIGUSR1);
    }



	/****************************************************************************
    *                         RECUPERATION FILES DE MESSAGES
    ****************************************************************************/

    // Recuperation file de message et on met son ID dans file_messCM 
    file_messCM = msgget(cleCM,0);
    if (file_messCM==-1){
        couleur(REINIT);
        fprintf(stderr,"Pb recuperation file de message\n");
        raise(SIGUSR1);
    }


    /****************************************************************************
    *                         RECUPERATION DES SEMAPHORES
    ****************************************************************************/


    // On cree le SMP et on teste s'il existe deja et on met son ID dans mem_part
    mem_part = shmget(cleSeg,sizeof(int[4]),0);
    if (mem_part == -1){
        couleur(REINIT);
		fprintf(stderr,"Pb recuperataion SMP\n");
		raise(SIGUSR1);
    }

    // Attachement de la memoire partagee et on met son @ dans adr_SEG    
    adr_SEG = shmat(mem_part,NULL,0);
    if (adr_SEG == (int *) -1){
        couleur(REINIT);
		fprintf(stderr,"Pb attachement memoire\n");
		shmctl(mem_part,IPC_RMID,NULL);   		// Il faut detruire le SMP puisqu'on l'a cree
		raise(SIGUSR1);
    }

    // recuperation de la semaphore et on met son ID dans semap
    semap = semget(cleSeg,4,0);
    if (semap == -1){
        couleur(REINIT);
		fprintf(stderr,"Pb recuperation Semaphore \n");
		shmctl(mem_part,IPC_RMID,NULL);     	 // Il faut detruire le SMP puisqu'on l'a cree 
												 // Le detachement du semaphore se fera a la terminaison
		raise(SIGUSR1);
    }

    mon_sigaction(SIGUSR1,arret);

    /****************************************************************************
    *                        	 BOUCLE D'ATTENTE
    ****************************************************************************/

    for(;;){
    	if((msgrcv(file_messCM,&req_travail,sizeof(requete_travail_de_chef_t)-sizeof(long),1,0)) == -1){
            couleur(REINIT);
            fprintf(stderr,"(Mecano) messages pas recus\n");
            perror("msgrcv");
            raise(SIGUSR1);
        }else {
            couleur(BLEU);
            fprintf(stdout,"\t\t\t(Mecano) Mecano n°%d : recoit un message de %d \n",num_ordre,req_travail.chef);

            duree_requise = req_travail.duree;              // assigne toutes les variables du travail donné au programme 
            outils_requis[0] = req_travail.outil[0];
            outils_requis[1] = req_travail.outil[1];
            outils_requis[2] = req_travail.outil[2];
            outils_requis[3] = req_travail.outil[3];
            
            O(semap,0,1);
            couleur(BLEU);
            fprintf(stdout,"\t\t\t(Mecano) Mecano n°%d, attend d'avoir tout ses outils a disposition\n\t\t\t\toutil 1 : %d, outil 2 : %d, outil 3 : %d, outil 4 : %d \n\n",num_ordre,adr_SEG[0],adr_SEG[1],adr_SEG[2],adr_SEG[3]);
            F(semap,0,1);
            while(tout_bon != 1){                           // on commence l'attente pour avoir les outils a disposition
                O(semap,0,1);
                if(adr_SEG[0] >= outils_requis[0] && adr_SEG[1] >= outils_requis[1] && adr_SEG[1] >= outils_requis[1] && adr_SEG[1] >= outils_requis[1]){
                    couleur(BLEU);
                    fprintf(stdout,"\t\t\t(Mecano) Mecano n°%d, a tout les outils necessaires a disposition pour travailler\n\t\t\t\tPrend : outil 1 : %d, outil 2 : %d, outil3 : %d, outil 4 : %d \n",num_ordre,outils_requis[0],outils_requis[1],outils_requis[2],outils_requis[3]);

                    // On prend les outils dans la boiit a outils
                    adr_SEG[0] = adr_SEG[0] - outils_requis[0];   
                    adr_SEG[1] = adr_SEG[1] - outils_requis[1];
                    adr_SEG[2] = adr_SEG[2] - outils_requis[2];
                    adr_SEG[3] = adr_SEG[3] - outils_requis[3];
                    tout_bon = 1;
                    couleur(BLEU);
                    fprintf(stdout,"\t\t\t\tIl reste : outil 1 : %d, outil 2 : %d, outil3 : %d, outil 4 : %d \n",adr_SEG[0],adr_SEG[1],adr_SEG[2],adr_SEG[3]);
                }
                F(semap,0,1);
            }

            usleep(1000000*duree_requise);                  // Le mecano execute son travail pendant duree_requise temps avant de rendre les outils 

            O(semap,0,1);
            adr_SEG[0] = adr_SEG[0] + outils_requis[0];     // Il remet les outils dans la boite a outils 
            adr_SEG[1] = adr_SEG[1] + outils_requis[1];
            adr_SEG[2] = adr_SEG[2] + outils_requis[2];
            adr_SEG[3] = adr_SEG[3] + outils_requis[3];
            couleur(BLEU);
            fprintf(stdout,"\t\t\t(Mecano) Mecano n°%d repose les outils\n\t\t\t\toutil 1 : %d, outil 2 : %d, outil3 : %d, outil 4 : %d \n\n",num_ordre,adr_SEG[0],adr_SEG[1],adr_SEG[2],adr_SEG[3]);
            F(semap,0,1);

            couleur(BLEU);
            fprintf(stdout,"\t\t\t(Mecano) Mecano n°%d, renvoie une reponse de son travail\n",num_ordre);
            rep_travail.type = req_travail.chef;            // Il prepare sa reponse au chef en mettant le type du chef pour que celui ci puisse le retrouver
            rep_travail.client = req_travail.client;
            rep_travail.mecano = pid;                  
            if((msgsnd(file_messCM,&rep_travail,sizeof(reponse_travail_de_mecanos_t)-sizeof(long),0)) != -1){
                couleur(BLEU);
                fprintf(stdout,"\t\t\t(Mecanos) Message envoyé a chefs avec succes ! \n");
            }
            tout_bon = 0;
            // envoie message de reponse dans la file de discussion Chef Mecano
        }
    }
    exit(0);
}