/**********************************************************************************
 * 
 * Projet de Sylvain Rome 
 * pour Programmation systeme 
 * DATE : 16 DECEMBRE 2021
 * 
 * *******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/msg.h>
#include <time.h>

#define FICHIER_CLE_SEG_OUT "cleSegOut.serv"
#define FICHIER_CLE_SEG_PRIO "cleSegPrio.serv"
#define FICHIER_CLE_FILECM "cleFile1.serv"
#define FICHIER_CLE_FILECC "cleFile2.serv"

#define MAX_CHEFS 20
#define MAX_MECANOS 60
#define MAX_CLIENTS 100



typedef pid_t Tableau_chefs[MAX_CHEFS];
typedef pid_t Tableau_mecanos[MAX_MECANOS];
typedef pid_t Tableau_clients[MAX_CLIENTS];

typedef struct{
	long type;
	pid_t chef;
	pid_t client;
	int duree;
	int outil[4];
}requete_travail_de_chef_t;

typedef struct{
	long type;
	pid_t client;
	pid_t mecano;
}reponse_travail_de_mecanos_t;

typedef struct{
	long type;
	pid_t client;
}requete_de_client_t;

typedef struct{
	long type;
	pid_t mecano;
}reponse_client_t;



/* Couleurs dans xterm */
#define couleur(param) fprintf(stdout,"\033[%sm",param)

#define NOIR  "30"
#define ROUGE "31"
#define VERT  "32"
#define JAUNE "33"
#define BLEU  "34"
#define CYAN  "36"
#define BLANC "37"
#define REINIT "0"